#include "interp.h"
#include <Arduino.h>

Interp interp;
int test = 0;



void processCommand() {
  if (!Serial.available())
    return;
  String line = Serial.readStringUntil('\n');
  Serial.println(interp.process(line));
}

void setup() {
  Serial.begin(115200);
  Serial.println("Generic Interp Device");
  
  interp.attatch("custom_led", [](StaticJsonDocument<2048> &req, StaticJsonDocument<2048> &res){
      test = req["num"];
  });

}



void loop() {
  processCommand();
  delay(5);
}