#ifndef __INTERP_H__
#define __INTERP_H__

#include <Adafruit_NeoPixel.h>
#include <Arduino.h>
#include <ArduinoJson.h>

#if (CONFIG_IDF_TARGET_ESP32S2 || CONFIG_IDF_TARGET_ESP32S3)
// we don't need the lib
#else
//#include <analogWrite.h>
#endif

#define INTERP_BUF_SIZE 2048




class Interp {
private:
  struct led_helper
{
  uint16_t width;
  uint16_t furthest_led;
  uint16_t fps;
  uint16_t duration;
  uint32_t colors[1000];
  int16_t leds[1000];

}led_state;
  TaskHandle_t lth;

  uint32_t pixcolor[1000];
  void LEDClearRegion(int16_t begin, int16_t end);
  void LEDSetRegion(int16_t begin, int16_t end, uint32_t color);
  void LEDGetColor(uint32_t* v);
  static float interpolatorLinear(float t, float p);
  static uint32_t mapColor(uint32_t init_color, uint32_t final_color, float factor);
  void (* callback)(StaticJsonDocument<INTERP_BUF_SIZE> &req, StaticJsonDocument<INTERP_BUF_SIZE> &res) = nullptr;
  String custom_action;
  SemaphoreHandle_t counter_semaphore;
  
  inline long hstol(String recv) {
    char c[recv.length() + 1];
    recv.toCharArray(c, recv.length() + 1);
    return strtol(c, NULL, 16);
  }

  int counter = 0;
  StaticJsonDocument<INTERP_BUF_SIZE> req;
  void setError(int code, String msg);
  void clearError();
  inline String resJson(bool pretty = false) {
    String outStr;
    res["_dt"] = millis() - res["_t"].as<unsigned long>();
    serializeJson(res, outStr);
    return outStr;
  }

  
  size_t _fillLEDsArray(StaticJsonDocument<INTERP_BUF_SIZE> &req);
  size_t _fillColorArray(StaticJsonDocument<INTERP_BUF_SIZE> &req);
  bool populateArrays(StaticJsonDocument<INTERP_BUF_SIZE> &req);

public:
  Interp();
  StaticJsonDocument<INTERP_BUF_SIZE> res;

  inline void attatch(String action, void (* cb)(StaticJsonDocument<INTERP_BUF_SIZE> &req, StaticJsonDocument<INTERP_BUF_SIZE> &res) ) {
    this->custom_action = action;
    callback = cb; 
  };
  Adafruit_NeoPixel pix = Adafruit_NeoPixel(100, 255, NEO_GRB + NEO_KHZ800);
  String process(String json);
  void LEDOutIn();
  void LEDInOut();
  void LEDLeftRight();
  void LEDRightLeft();
  DynamicJsonDocument newMessage(DynamicJsonDocument document);

};



#endif // __INTERP_H__