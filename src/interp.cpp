#include "interp.h"

#define ACTION_DW "dw"
#define ACTION_DR "dr"
#define ACTION_AW "aw" // pwm
#define ACTION_AR "ar"
#define ACTION_SERVO "servo"
#define ACTION_DELAY "delay"
#define ACTION_MILLIS "millis"
#define ACTION_PRINTLN "println"
#define ACTION_READLN "readln"
#define ACTION_REBOOT "reboot"
#define ACTION_LED "led"
#define ACTION_LED_INOUT "led_inout"
#define ACTION_LED_OUTIN "led_outin"
#define ACTION_LED_LEFTRIGHT "led_lr"
#define ACTION_LED_RIGHTLEFT "led_rl"
#define ACTION_LED_SET_REGION "led_set_region"
#define ACTION_LED_CLEAR "led_clear"

#define ERR_BAD_REQUEST 4000

Interp::Interp() {
  counter_semaphore = xSemaphoreCreateMutex();
  led_state.furthest_led = pix.numPixels();
}

void Interp::setError(int code, String msg) {
  res["errorCode"] = code;
  res["error"] = msg;
  res["status"] = "error";
}
void Interp::clearError() {
  res.remove("errorCode");
  res.remove("error");
  res["status"] = "ok";
}



String Interp::process(String json) {
  String outStr;
  req.clear();

  res.clear();
  res["status"] = "ok";
  xSemaphoreTake(counter_semaphore, portMAX_DELAY);
  res["_msgId"] =
      "i" + String(counter++); // just to make sure nobody parses it as an int
  xSemaphoreGive(counter_semaphore);
  res["_t"] = millis();


  DeserializationError error = deserializeJson(req, json);
  if (error) {
    Serial.println(error.f_str());
    setError(ERR_BAD_REQUEST, "deserialization error");
    return resJson();
  }

  // Send ack to msg
  if (req.containsKey("_msgId")) {
    res["_ackId"] = req["_msgId"];
  }
  // Most of the time, you can rely on the implicit casts.
  // In other case, you can do doc["time"].as<long>();
  String action((const char *)req["action"]);

  if (action == ACTION_DW) {
    int pin = req["pin"];
    bool value = req["value"];
    pinMode(pin, OUTPUT);
    digitalWrite(pin, value ? HIGH : LOW);
    return resJson();
  }

  if (action == ACTION_AW) {
    int pin = req["pin"];
    int duty = req["duty"];
    res["_duty"] = duty;
    pinMode(pin, OUTPUT);
    analogWrite(pin, duty);
    return resJson();
  }

  // we're running a software servo, check out
  // https://github.com/adafruit/Adafruit_SoftServo/blob/master/Adafruit_SoftServo.cpp
  // we might want to consider a range outside [1000, 2000] microseconds, since
  // most cheap servos work with ranges like [800, 2500] maybe have a
  // multiplier?
  if (action == ACTION_SERVO) {
    int pin = req["pin"];
    int angle = req["angle"] | 90;
    int delayMillis = req["delayMillis"] | 1000;
    pinMode(pin, OUTPUT);

    unsigned long start_millis = millis();
    while (millis() - start_millis < delayMillis) {
      digitalWrite(pin, HIGH);
      // should be 1000 to 2000 but yeah some servos dont rly work like that
      int pulse = map(angle, 0, 180, 1000, 2000);
      res["_pulse"] = pulse;
      res["_delay"] = delayMillis;
      delayMicroseconds(map(angle, 0, 180, 1000, 2000));
      digitalWrite(pin, LOW);
      delay(2);
    }
    // {"action":"dr","pin":2}
    // {"action":"servo","pin":2, "angle":-20, "delayMillis":800}
    // {"action":"servo","pin":2, "angle":90, "delayMillis":800}
    // {"action":"servo","pin":2, "angle":180, "delayMillis":800}

    return resJson();
  }

  if (action == ACTION_AR) {
    res["pin"] = (int)req["pin"];
    int pin = (int)req["pin"];
    int mode = ((bool)req["pullDown"] | false)  ? INPUT_PULLDOWN
               : ((bool)req["pullAsd"] | false) ? INPUT_PULLUP
                                                : INPUT;
    Serial.println(mode);
    pinMode(pin, mode);
    res["value"] = analogRead(pin);
    return resJson();
  }

  if (action == ACTION_DR) {
    int pin = (int)req["pin"];
    res["pin"] = pin;
    int mode = ((bool)req["pullDown"] | false)  ? INPUT_PULLDOWN
               : ((bool)req["pullAsd"] | false) ? INPUT_PULLUP
                                                : INPUT;
    Serial.println(mode);

    pinMode(pin, mode);
    res["value"] = digitalRead((int)req["pin"]) ? true : false;
    return resJson();
  }

  if (action == ACTION_DELAY) {
    delay((int)req["delay"]);
    return resJson();
  }

  if (action == ACTION_MILLIS) {
    res["millis"] = millis();
    return resJson();
  }

  if (action == ACTION_PRINTLN) {
    if (!req.containsKey("text")) {
      setError(ERR_BAD_REQUEST, "missing text argument");
      return resJson();
    }
    Serial.println(String((const char *)req["text"]));
    Serial.flush();
    return resJson();
  }

  if (action == ACTION_READLN) {
    int timeout = req["timeout"] | 1000;
    unsigned long prevTimeout = Serial.getTimeout();
    Serial.setTimeout(timeout);
    String s = Serial.readStringUntil('\n');
    Serial.setTimeout(prevTimeout);
    res["text"] = s;
    return resJson();
  }

#ifdef ESP_PLATFORM
  if (action == ACTION_REBOOT)
  {
    ESP.restart();
    return "";
  }
#endif
  

  if (action == ACTION_LED) {
    const int pin = req["pin"];
    JsonArray values = req["values"];

    pix.setPin(pin);
    if (values.size() != pix.numPixels()) {
      pix.updateLength(values.size());
      res["_realloc"] = true;
    }
    for (int i = 0; i < values.size(); i++) {
      int color = values[i];
      auto c =
          pix.Color(color >> 16 & 0xff, color >> 8 & 0xff, color >> 0 & 0xff);
      pix.setPixelColor(i, c);
    }
    pix.show();
    // FastLED.show();
    res["numLeds"] = values.size();
    return resJson();
  }


  //{"action":"led_inout","pin":35, "begin": 22, "width": 7, "color": 2015241, "duration_ms": 100}
  //{"action":"led_inout","pin":35, "leds": [5, 7, 8, 9, 14, 20], "color": 64 , "duration_ms": 100}
  //{"action":"led_inout","pin":36, "leds": [5, 7, 8, 9, 14, 20], "colors": [65280, 16646144, 255, 65280, 16646144, 255], "duration_ms": 3000}

  if (action == ACTION_LED_INOUT){
    const int pin = req["pin"];
    led_state.fps = req["fps"] | 30;
    led_state.duration = req["duration_ms"];
    if(populateArrays(req)){
      setError(ERR_BAD_REQUEST, "Color and LED array sizes do not match");
      return resJson();
    }
    if(pix.getPin() != pin)
      pix.setPin(pin);
    if (lth != nullptr){
        vTaskDelete(lth);
        lth = nullptr;
    }
    
    auto r = xTaskCreate(
      [](void* o){static_cast<Interp*>(o)->LEDInOut();},
      "LED Controller",
      4096,
      this,
      tskIDLE_PRIORITY+1,
      &lth
    );
    return resJson();

  //{"action":"led_outin","pin":35, "begin": 22, "width": 7, "color": 2015241, "duration_ms": 20}

  }  
  
  if (action == ACTION_LED_OUTIN){
    const int pin = req["pin"];
    if(populateArrays(req)){
      setError(ERR_BAD_REQUEST, "Color and LED array sizes do not match");
      return resJson();
    }
    led_state.fps = req["fps"] | 30;
    led_state.duration = req["duration_ms"];
    if(pix.getPin() != pin)
      pix.setPin(pin);
    if (lth != nullptr){
        vTaskDelete(lth);
        lth = nullptr;
    }
    
    auto r = xTaskCreate(
      [](void* o){static_cast<Interp*>(o)->LEDOutIn();},
      "LED Controller",
      4096,
      this,
      tskIDLE_PRIORITY+1,
      &lth
    );
    return resJson();
  }

  if (action == ACTION_LED_LEFTRIGHT){
    const int pin = req["pin"];
    if(populateArrays(req)){
      setError(ERR_BAD_REQUEST, "Color and LED array sizes do not match");
      return resJson();
    }
    led_state.fps = req["fps"] | 30;
    led_state.duration = req["duration_ms"];
    if(pix.getPin() != pin)
      pix.setPin(pin);
    if (lth != nullptr){
        vTaskDelete(lth);
        lth = nullptr;
    }
    
    auto r = xTaskCreate(
      [](void* o){static_cast<Interp*>(o)->LEDLeftRight();},
      "LED Controller",
      4096,
      this,
      tskIDLE_PRIORITY+1,
      &lth
    );
    return resJson();
  }

  if (action == ACTION_LED_RIGHTLEFT){
    const int pin = req["pin"];
    if(populateArrays(req)){
      setError(ERR_BAD_REQUEST, "Color and LED array sizes do not match");
      return resJson();
    }
    led_state.fps = req["fps"] | 30;
    led_state.duration = req["duration_ms"];
    if(pix.getPin() != pin)
      pix.setPin(pin);
    if (lth != nullptr){
        vTaskDelete(lth);
        lth = nullptr;
    }
    
    auto r = xTaskCreate(
      [](void* o){static_cast<Interp*>(o)->LEDRightLeft();},
      "LED Controller",
      4096,
      this,
      tskIDLE_PRIORITY+1,
      &lth
    );
    return resJson();
  }

  if (action == ACTION_LED_CLEAR){
    const int pin = req["pin"];
    const int begin = req["begin"];
    int end = req["width"]; end += begin;
    if(pix.getPin() != pin)
      pix.setPin(pin);
    if ( end > pix.numPixels()) {
      pix.updateLength(end);
      res["_realloc"] = true;
    }
    LEDClearRegion(begin, end);
    pix.show();
    return resJson();
  }  
  
  if (action == ACTION_LED_SET_REGION){
    const int pin = req["pin"];
    const int begin = req["begin"];
    int width = req["width"];
    const uint32_t color = req["color"];
    if(pix.getPin() != pin)
      pix.setPin(pin);
    LEDSetRegion(begin, begin + width, color);
    pix.show();
    return resJson();
  }

  if (action == custom_action and callback != nullptr){
    callback(req, res);
    return resJson();
  }

  setError(ERR_BAD_REQUEST, "unknown command");
  return resJson();
}

// Returns the final size of the LEDs array
size_t Interp::_fillLEDsArray(StaticJsonDocument<INTERP_BUF_SIZE> &req){
  //We will not have schema validation anytime soon:
  //https://github.com/bblanchon/ArduinoJson/issues/781#issuecomment-405489461

  uint32_t begin = req["begin"] | __UINT32_MAX__;
  uint32_t width = req["width"] | __UINT32_MAX__;
  bool furthest_led_changed = false;
  if (begin == __UINT32_MAX__){
    width = copyArray(req["leds"], led_state.leds);
    for (size_t i = 0; i < width; i++){
      if (led_state.leds[i] > led_state.furthest_led){
        led_state.furthest_led = led_state.leds[i];
        furthest_led_changed = true;
      }
    }   
  }
  else {
    for (size_t i = 0; i < width; i++) {
      led_state.leds[i] = begin + i;
    }
    if (begin + width > led_state.furthest_led){
        led_state.furthest_led = begin + width;
        furthest_led_changed = true;
    }
  }
  led_state.width = width;


  if (furthest_led_changed){
    pix.updateLength(led_state.furthest_led);
  }
  return led_state.width;
}

// Returns the final size of the colors array. Should be called AFTER "_fillLEDsArray()" (or just use "populateArrays()")
size_t Interp::_fillColorArray(StaticJsonDocument<INTERP_BUF_SIZE> &req){
  uint32_t color = req["color"] | __UINT32_MAX__;
  size_t width;
  if (color == __UINT32_MAX__){
    width = copyArray(req["colors"], led_state.colors);
  }
  else{
    for (size_t i = 0; i < led_state.width; i++) {
      led_state.colors[i] = req["color"];
    }
    width = led_state.width;
  }
  return width;
}

// Returns whether the two arrays have the same size
bool Interp::populateArrays(StaticJsonDocument<INTERP_BUF_SIZE> &req){
  auto size1 = _fillLEDsArray(req);
  auto size2 = _fillColorArray(req);
  return size1 != size2;
}

void Interp::LEDSetRegion(int16_t begin, int16_t end, uint32_t color){
    for (size_t i = 0; i < end-begin; i++){
    pix.setPixelColor(begin+i, color);
  }
}

void Interp::LEDClearRegion(int16_t begin, int16_t end){
  LEDSetRegion(begin, end, 0x00);
    
}

void Interp::LEDOutIn(){
LEDGetColor(pixcolor);
  int total_frames = led_state.duration*1E-3*led_state.fps;
  auto last_call = xTaskGetTickCount();
  for (size_t frames_done = 1; frames_done <= total_frames; frames_done++){
    for (size_t j = 0; j < floor((led_state.width+1)/2); j++){
      float f = interpolatorLinear((float)(frames_done) / (float)(total_frames), (float)(j) / (float)(led_state.width));
      pix.setPixelColor(
          led_state.leds[j],
          mapColor(
            pixcolor[led_state.leds[j]],
            led_state.colors[j], 
            f));
        pix.setPixelColor(
          led_state.leds[led_state.width-j-1],
          mapColor(
            pixcolor[led_state.leds[led_state.width-j-1]],
            led_state.colors[led_state.width-j-1], 
            f));
    }
    pix.show();
    vTaskDelayUntil(&last_call, ((1E3/led_state.fps))/portTICK_PERIOD_MS);
  }  
  lth = nullptr;
  vTaskDelete(NULL);

} 

void Interp::LEDInOut(){
  LEDGetColor(pixcolor);
  int total_frames = led_state.duration*1E-3*led_state.fps;
  auto last_call = xTaskGetTickCount();
  for (size_t frames_done = 1; frames_done <= total_frames; frames_done++){
    for (size_t j = 0; j < floor((led_state.width+1)/2); j++){
      float f = interpolatorLinear((float)(frames_done) / (float)(total_frames), (float)(j) / (float)(led_state.width));
      pix.setPixelColor(
        led_state.leds[(led_state.width)/2 + j],
        mapColor(
          pixcolor[led_state.leds[(led_state.width)/2 + j]],
          led_state.colors[(led_state.width)/2 + j],
          f));
      pix.setPixelColor(
        led_state.leds[(led_state.width-1)/2 - j],
        mapColor(
          pixcolor[led_state.leds[(led_state.width-1)/2 - j]],
          led_state.colors[(led_state.width-1)/2 - j],
          f));
    }
    pix.show();
    vTaskDelayUntil(&last_call, ((1E3/led_state.fps))/portTICK_PERIOD_MS);
  }  
  lth = nullptr;
  vTaskDelete(NULL);
} 

void Interp::LEDLeftRight(){
  LEDGetColor(pixcolor);
  int total_frames = led_state.duration*1E-3*led_state.fps;
  auto last_call = xTaskGetTickCount();
  for (size_t frames_done = 1; frames_done <= total_frames; frames_done++){
    for (size_t j = 0; j < led_state.width; j++){
      float f = interpolatorLinear((float)(frames_done) / (float)(total_frames), (float)(j) / (float)(led_state.width));
      pix.setPixelColor(
        led_state.leds[j],
        mapColor(
          pixcolor[led_state.leds[j]],
          led_state.colors[j],
          f));
    }
    pix.show();
    vTaskDelayUntil(&last_call, ((1E3/led_state.fps))/portTICK_PERIOD_MS);
  }  
  lth = nullptr;
  vTaskDelete(NULL);
} 

void Interp::LEDRightLeft(){
  LEDGetColor(pixcolor);
  int total_frames = led_state.duration*1E-3*led_state.fps;
  auto last_call = xTaskGetTickCount();
  
  for (size_t frames_done = 1; frames_done <= total_frames; frames_done++){
    for (size_t j = 0; j < led_state.width; j++){
      float f = interpolatorLinear((float)(frames_done) / (float)(total_frames), (float)(j) / (float)(led_state.width));
      pix.setPixelColor(
        led_state.leds[led_state.width - j-1],
        mapColor(
          pixcolor[led_state.leds[led_state.width - j-1]],
          led_state.colors[led_state.width - j-1],
          f));
    }
    pix.show();
    vTaskDelayUntil(&last_call, ((1E3/led_state.fps))/portTICK_PERIOD_MS);
  }  
  lth = nullptr;
  vTaskDelete(NULL);
} 

void Interp::LEDGetColor(uint32_t* v){
  for (size_t i = 0; i < pix.numPixels(); i++){
    v[i] = pix.getPixelColor(i);
  }
}


uint32_t Interp::mapColor(uint32_t init_color, uint32_t final_color, float factor){
  uint8_t i_r, i_g, i_b, f_r, f_g, f_b, m_r, m_g, m_b;
  i_r = (init_color >> 16) & 0x0FF;
  i_g = (init_color >> 8) & 0x0FF;
  i_b = (init_color >> 0) & 0x0FF;
  f_r = (final_color >> 16) & 0x0FF;
  f_g = (final_color >> 8) & 0x0FF;
  f_b = (final_color >> 0) & 0x0FF;
  m_r = i_r + ((f_r - i_r)*factor);
  m_g = i_g + ((f_g - i_g)*factor);
  m_b = i_b + ((f_b - i_b)*factor);
  return (uint32_t)m_b | (uint32_t)(m_g << 8) | (uint32_t)(m_r << 16);
}

float Interp::interpolatorLinear(float t, float p) {
  if (t >= p)
    return t;
  else
    return 0.0;
}

DynamicJsonDocument Interp::newMessage(DynamicJsonDocument document){
  if (document.containsKey("_msgId"))
    document["_ackId"] = document["_msgId"];
  xSemaphoreTake(counter_semaphore, portMAX_DELAY);
  document["_msgId"] =
      "i" + String(counter++); // just to make sure nobody parses it as an int
  xSemaphoreGive(counter_semaphore);
  document["_t"] = millis();
  return document;
}
